import { Component } from '@angular/core';

@Component({
    selector: 'outlet-todo',
    templateUrl: './outlet-todo.component.html',
    styleUrls: ['./outlet-todo.component.scss']
})
export class OutletTodoComponent {}
